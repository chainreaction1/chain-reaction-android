import React from "react";
import CoreOffline from "../Core/CoreOffline";
import CoreOnlineFriend from "../Core/CoreOnlineFriend";
import CoreOnline from "../Core/CoreOnline";
import { withRouter } from "react-router-dom";
import { withCookies } from "react-cookie";

const GameScreen = (props) => {
  switch (props.gameScreenId) {
    case 1: {
      return <CoreOffline />;
    }
    case 2: {
      console.log(props);
      const secret = props.cookies.get("G:secret");
      const opponentName = props.cookies.get("G:opponentName");
      const myUserNum = props.cookies.get("G:myUserNum");
      const myName = props.cookies.get("G:myName");
      const turn = props.cookies.get("G:turn");
      const id = props.cookies.get("G:id");
      console.log(secret, opponentName, myUserNum, myName, turn, id);
      if (!myUserNum || !turn || !secret || !myName || !id || !opponentName) {
        props.history.push("/friend");
        return null;
      } else {
        return (
          <CoreOnlineFriend
            myUserNum={parseInt(myUserNum)}
            turn={parseInt(turn)}
            secret={secret}
            opponentName={opponentName}
            myName={myName}
            id={id}
          />
        );
      }
    }
    case 3: {
      const secret = props.cookies.get("G:secret");
      const opponentName = props.cookies.get("G:opponentName");
      const myUserNum = props.cookies.get("G:myUserNum");
      const myName = props.cookies.get("G:myName");
      const turn = props.cookies.get("G:turn");
      const id = props.cookies.get("G:id");
      console.log(secret, opponentName, myUserNum, myName, turn, id);
      if (!myUserNum || !turn || !secret || !myName || !id || !opponentName) {
        props.history.push("/");
        return null;
      } else {
        return (
          <CoreOnline
            myUserNum={parseInt(myUserNum)}
            turn={parseInt(turn)}
            secret={secret}
            opponentName={opponentName}
            myName={myName}
            id={id}
          />
        );
      }
    }
    default: {
      props.history.push("/");
      return null;
    }
  }
};

export default withCookies(withRouter(GameScreen));
